# AWS_Gitlab_CI_CD_Terraform

## Two-tier Architectures
![two-tier-architecture](twotier_architecht.png)

## Migrate to a GitLab-managed Terraform state

- In your Terraform project, in a .tf file like backend.tf, define the HTTP backend

```
terraform {
  backend "http" {
  }
}
```

### Set up the initial backend

PROJECT_ID="<gitlab-project-id>"
TF_USERNAME="<gitlab-username>"
TF_PASSWORD="<gitlab-personal-access-token>"
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/old-state-name"

terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5


## Getting started







