# Public subnet EC2 instance 1
resource "aws_instance" "web-server-1" {
  ami             = "ami-064eb0bee0c5402c5"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.two-tier-ec2-sg.id]
  subnet_id       = aws_subnet.public-subnet-a.id
  key_name   = "two-tier-key"

  tags = {
    Name = "web-server-1"
  }

  user_data = <<-EOF
#!/bin/bash
sudo yum update -y
sudo yum install -y httpd
sudo systemctl enable httpd
sudo service httpd start  
sudo echo '<h1>Welcome to APP-1</h1>' | sudo tee /var/www/html/index.html
sudo mkdir /var/www/html/app1
sudo echo '<!DOCTYPE html> <html> <body style="background-color:rgb(250, 210, 210);"> <h1>Welcome to APP-1</h1> <p>Terraform Demo</p> <p>Application Version: V1</p> </body></html>' | sudo tee /var/www/html/app1/index.html
sudo curl http://169.254.169.254/latest/dynamic/instance-identity/document -o /var/www/html/app1/metadata.html
EOF
}

# Public subnet  EC2 instance 2
resource "aws_instance" "web-server-2" {
  ami             = "ami-064eb0bee0c5402c5"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.two-tier-ec2-sg.id]
  subnet_id       = aws_subnet.two-tier-pub-sub-2.id
  key_name   = "two-tier-key"

  tags = {
    Name = "web-server-2"
  }

  user_data = <<-EOF
#!/bin/bash
sudo yum update -y
sudo yum install -y httpd
sudo systemctl enable httpd
sudo service httpd start  
sudo echo '<h1>Welcome to APP-1</h1>' | sudo tee /var/www/html/index.html
sudo mkdir /var/www/html/app1
sudo echo '<!DOCTYPE html> <html> <body style="background-color:rgb(250, 210, 210);"> <h1>Welcome to APP-1</h1> <p>Terraform Demo</p> <p>Application Version: V1</p> </body></html>' | sudo tee /var/www/html/app1/index.html
sudo curl http://169.254.169.254/latest/dynamic/instance-identity/document -o /var/www/html/app1/metadata.html
EOF
}

#EIP

resource "aws_eip" "web-server-1-eip" {
  domain = "vpc"

  instance                  = aws_instance.web-server-1.id
  depends_on                = [aws_internet_gateway.two-tier-igw]
}

resource "aws_eip" "web-server-2-eip" {
  domain = "vpc"

  instance                  = aws_instance.web-server-2.id
  depends_on                = [aws_internet_gateway.two-tier-igw]
}